package jbdc.client;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DbConnection{
    private static DbConnection dbConnection;
    
    public static Connection connection = null;
    
    private static EntityManagerFactory emf = null;
    
    static String Host;
    static int Port;
    static String Database;
    static String Name;
    static String Password;
    
    String dburl;
    
    public DbConnection DbConnection() {
        if (dbConnection == null) {
            dbConnection = new DbConnection();
        }
        return dbConnection;
    }

    static public void newConnection(String host, int port, String database, String name, String password) {
        String url = String.format("jdbc:postgresql://%s:%s/%s", host, port, database);
        if (connection != null) {
            System.out.println("connection exist");
            return;
        }

        Host = host;
        Port = port;
        Database = database;
        Name = name;
        Password = password;

        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, name, password);

            // Включает логгироване в файл
            PrintWriter pw = new PrintWriter("C:\\Users\\user\\Downloads\\Учеба и работа\\субд\\субд\\labs\\ExampleJDBC\\logs\\jdbc.log");
            DriverManager.setLogWriter(pw);

            JDBCClient.setStatusMessage(
                    String.format("Соединение с базой %s установлено",
                                connection.getMetaData().getURL()));
        } catch (ClassNotFoundException | SQLException ex) {
            JDBCClient.setStatusMessage(ex.getMessage());
            Logger.getLogger(JDBCClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DbConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    static public EntityManagerFactory getEMF() {
        if (emf == null) {
            emf = Persistence.createEntityManagerFactory("jdncclient");
        }

        return emf;
    }
   
static public void closeConnection() {
    if (connection != null) {
        try {
            String url = connection.getMetaData().getURL();
            connection.close(); // Каскадное закрытие statement, resultSet
            connection = null;

            JDBCClient.setStatusMessage(
                    String.format("Соединение с базой %s разорвано", url));
        } catch (SQLException ex) {
            JDBCClient.setStatusMessage(ex.getMessage());
            Logger.getLogger(JDBCClient.class.getName()).log(
                    Level.SEVERE,
                    null, ex);
        }
    }
}
    
    public List<String> getDatabaseTables(String database) {

        List<String> tables = new ArrayList<>();
        try {
            DatabaseMetaData dbmd = connection.getMetaData();
            String[] types = {"TABLE"};
            ResultSet rs = dbmd.getTables(database, null, "%", types);
            while (rs.next()) {
                tables.add(rs.getString("TABLE_NAME"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(JDBCClient.class.getName()).log(Level.SEVERE, null,
                    ex);
        } 
        return tables;
    }
    
 
    public static String[][] getAllFromTable(String tableName) {
       
        String[][] selectResult = new String[][]{};

        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(String.format("SELECT * from %s;", tableName));

            ResultSetMetaData rsmd = rs.getMetaData();
            int columnsCnt = rsmd.getColumnCount();

            String[] eArrHeader = new String[columnsCnt];
            for (int n = 0; n < columnsCnt; n++) {
                eArrHeader[n]  = rsmd.getColumnLabel(n+1);
            }
            selectResult = Arrays.copyOfRange(selectResult, 0, selectResult.length + 1);
            selectResult[0] = eArrHeader;

            int rowCnt = 1;
            while (rs.next()) {
                String[] row = new String[columnsCnt];
                for (int n = 1; n <= columnsCnt; n++) {
                    row[n-1] = rs.getString(rsmd.getColumnLabel(n));
                }

                selectResult = Arrays.copyOfRange(selectResult, 0, selectResult.length + 1);
                selectResult[rowCnt] = row;

                rowCnt++;
            }
        } catch (SQLException ex) {
            JDBCClient.setStatusMessage(ex.getMessage());
            Logger.getLogger(JDBCClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return selectResult;
    }

    public String[] getTableColumns(String tableName) {
        String[] tableHeader = null;
         try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(String.format("SELECT * from %s;", tableName));
            
            ResultSetMetaData rsmd = rs.getMetaData();

            
            int columnsCnt = rsmd.getColumnCount();
            tableHeader = new String[columnsCnt];
            for (int n = 0; n < columnsCnt; n++) {
                tableHeader[n] = rsmd.getColumnLabel(n+1);
            }
        } catch (SQLException ex) {
            JDBCClient.setStatusMessage(ex.getMessage());
            Logger.getLogger(JDBCClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return tableHeader;
    }
    
    public int getColumnsCnt(String tableName) {
        int columnsCnt = -1;
         try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(String.format("SELECT * from %s LIMIT 1;", tableName));

            ResultSetMetaData rsmd = rs.getMetaData();
            columnsCnt = rsmd.getColumnCount();
        } catch (SQLException ex) {
            JDBCClient.setStatusMessage(ex.getMessage());
            Logger.getLogger(JDBCClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return columnsCnt;
    }

    public void createEm() {
        if (emf == null) {
            emf = getEMF();
        }
        EntityManager em = emf.createEntityManager();
    }
    
//    public static void createDatabase() {

//    }
}