/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbdc.client;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;


public class Employee implements Serializable {

    final static private String tableName = "employee";
    
    private Integer id;
    private String firstName;
    private String lastName;
    private String phoneNo;
    private String position;
    private Date employmentDate;

    public Employee() {
    }

    public Employee(Integer id) {
        this.id = id;
    }

    public Employee(Integer id, String firstName, String lastName, String phoneNo, String position, Date employmentDate) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNo = phoneNo;
        this.position = position;
        this.employmentDate = employmentDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Date getEmploymentDate() {
        return employmentDate;
    }

    public void setEmploymentDate(Date employmentDate) {
        this.employmentDate = employmentDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Employee)) {
            return false;
        }
        Employee other = (Employee) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    public void insert() throws SQLException {
        PreparedStatement pstmt = DbConnection.connection.prepareStatement(
            "INSERT INTO " + getTableName()
            + " (first_name, last_name, phone_no, position, employment_date) "
            + "VALUES (?, ?, ?, ?, ?)",
            ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

        pstmt.setString(1, getFirstName());
        pstmt.setString(2,getLastName());
        pstmt.setString(3, getPhoneNo());
        pstmt.setString(4, getPosition());
        java.sql.Date eD = UtilityMethods.convertDateToSqlDate(getEmploymentDate());
        pstmt.setDate(5, eD);

        pstmt.executeUpdate();
        pstmt.close();
    }

    public void update() throws SQLException {
        PreparedStatement ps = DbConnection.connection.prepareStatement(
                "SELECT * FROM " + getTableName() + " WHERE id=?");
        ps.setInt(1, getId());

        ResultSet uprs = ps.executeQuery();

        uprs.next();
        uprs.updateString("first_name", getFirstName());
        uprs.updateString("last_name",getLastName());
        uprs.updateString("phone_no", getPhoneNo());
        uprs.updateString("position", getPosition());
        java.sql.Date eD = UtilityMethods.convertDateToSqlDate(getEmploymentDate());
        uprs.updateDate("employment_date", eD);

        uprs.updateRow(); 
        uprs.beforeFirst();

        ps.close();
    }

    public void deteleEmployee() throws SQLException {
        PreparedStatement pstmt = DbConnection.connection.prepareStatement(
                        "DELETE FROM " + getTableName() + " WHERE id=?");
        pstmt.setInt(1, getId());
        pstmt.executeUpdate();
    }
    
    
    @Override
    public String toString() {
        return "examplejdbc.Employee_1[ "
                + "id=" + id 
                + " firstName=" + firstName 
                + " lastName=" + lastName 
                + " phoneNo=" + phoneNo 
                + " position=" + position 
                + " employmentDate=" + employmentDate 
                + " ]";
    }
    
    public static String getTableName() {
        return tableName;
    }
}
