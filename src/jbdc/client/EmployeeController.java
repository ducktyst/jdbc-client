/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbdc.client;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Table;


public class EmployeeController {
     
    private static String getTableName() {
        return Employee.getTableName();
    }
     
    public static Employee getEmployee(int id) {

        Employee employee = new Employee();
        try {
            PreparedStatement statement = DbConnection.connection.prepareStatement(
                    "SELECT * FROM "+ getTableName() +" WHERE employee.id = ?");
            statement.setInt(1, id);

            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                employee.setId(rs.getInt("id"));
                employee.setFirstName(rs.getString("first_name"));
                employee.setLastName(rs.getString("last_name"));
                employee.setPhoneNo(rs.getString("phone_no"));
                employee.setPosition(rs.getString("position"));
                employee.setEmploymentDate(rs.getDate("employment_date"));
            }
        } catch (SQLException ex) {
            JDBCClient.setStatusMessage(ex.getMessage());
            Logger.getLogger(JDBCClient.class.getName()).log(Level.SEVERE, null, ex);
        }

        return employee;
    };

    public static void deteleEmployee(int id) {
        Class<?> c = Employee.class;
        Table table = c.getAnnotation(Table.class);
        String tableName = table.name();

        PreparedStatement stmt = null;
        try {
            stmt = DbConnection.connection.prepareStatement(
                    "DELETE FROM " + getTableName() + " WHERE id=?");
            stmt.setInt(1, id);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            JDBCClient.setStatusMessage(ex.getMessage());
            Logger.getLogger(JDBCClient.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (stmt != null) { 
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DbConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
