package jbdc.client;

import java.awt.Component;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JMenuItem;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

public class JDBCClient extends javax.swing.JFrame {
    static DbConnection conn = new DbConnection();
    static MainFrame mf = new MainFrame();
    
    public static DbConnection getConn() {
        return conn;
    }
    
    public static void updateMenuTables(String database) {
        javax.swing.JMenu menuTables = mf.getjMenuTables();
        menuTables.removeAll();
        List<JMenuItem> tableMenuItems = new ArrayList<>();

        List<String> tables = conn.getDatabaseTables(database);
        for (String table : tables) {
            JMenuItem newItem = new JMenuItem();

            newItem.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    long t1 = System.nanoTime();
                    System.out.println("Загрузка из справочника "+ table +". Старт ; " + t1);

                    JMenuItem item = (JMenuItem)evt.getSource();
                    String tableName = item.getText();
                    updateTable(tableName);
                    mf.jLabelTableName.setText(tableName);

                    mf.openTableTab(evt);

                    long t2 = System.nanoTime();
                    System.out.println("Загрузка из справочника "+ table +". Фин ; " + t2);
                    System.out.println("Выполнение. ; " + (t2-t1) + "; нс.");
                }
            });

            newItem.setText(table);
            menuTables.add(newItem);
        }
        mf.setjMenuTables(menuTables);
    }
    
    public static void updateTable(String tableName) {
        JTable mainTable = JDBCClient.getMainJTable();

        String[][] tableContent = conn.getAllFromTable(tableName);
        String[] tableHeader = tableContent[0];
        String[][] tableBody = Arrays.copyOfRange(tableContent, 1, tableContent.length);

        javax.swing.table.DefaultTableModel tableModel = new javax.swing.table.DefaultTableModel(
            tableBody,
            tableHeader
        ){
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return false;
            }
        };

        mainTable.setModel(tableModel);
        autoSizeTableColumns(mainTable);
    }

    public static JTable getMainJTable() {
        return mf.jTableMain; // todo: getter mf.getJtable()
    }

    public static void reloadSelectedTableContent() {
        String selectedTable = mf.jLabelTableName.getText();
        JDBCClient.updateTable(selectedTable);        
    }
    public static void autoSizeTableColumns(JTable table) {

        TableModel  model        = table.getModel();
        TableColumn column       = null;
        Component   comp         = null;
        int         headerWidth  = 0;
        int         maxCellWidth = Integer.MIN_VALUE;
        int         cellWidth    = 0;
        TableCellRenderer headerRenderer =
            table.getTableHeader().getDefaultRenderer();

        for (int i = 0; i < table.getColumnCount(); i++) {
            column = table.getColumnModel().getColumn(i);
            comp = headerRenderer.getTableCellRendererComponent(table,
                    column.getHeaderValue(), false, false, 0, 0);
            headerWidth  = comp.getPreferredSize().width + 10;
            maxCellWidth = Integer.MIN_VALUE;

            for (int j = 0; j < Math.min(model.getRowCount(), 30); j++) {
                TableCellRenderer r = table.getCellRenderer(j, i);

                comp = r.getTableCellRendererComponent(table,
                                                       model.getValueAt(j, i),
                                                       false, false, j, i);
                cellWidth = comp.getPreferredSize().width;

                if (cellWidth >= maxCellWidth) {
                    maxCellWidth = cellWidth;
                }
            }

            column.setPreferredWidth(Math.max(headerWidth, maxCellWidth)
                                     + 10);
        }
    }

    public static void main(String[] args) {
        // TODO: вводить данные в форме "Новое соединение"
        String host = "127.0.0.1";
        int port = 5432;
        String database = "hotel_db";
        String name = "postgres";
        String password = "postgres";
        DbConnection.newConnection(host, port, database, name, password);
        JDBCClient.updateMenuTables(database);

        mf.setVisible(true);

        mf.jTableMain.setRowSelectionAllowed(true);
        mf.jTableMain.setColumnSelectionAllowed(false);
        mf.jTableMain.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);
    }

    static void setStatusMessage(String message) {
        mf.setStatusBarMessage(message);
    }
}
