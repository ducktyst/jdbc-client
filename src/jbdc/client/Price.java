/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbdc.client;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class Price implements Serializable {
    final static private String tableName = "price";

    private int pricePerDay;
    private int period;
    private Integer id;

    public Price() {
    }

    public Price(Integer id) {
        this.id = id;
    }

    public Price(Integer id, int pricePerDay, int period) {
        this.id = id;
        this.pricePerDay = pricePerDay;
        this.period = period;
    }

    public int getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(int pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Price)) {
            return false;
        }
        Price other = (Price) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    
    
    public void insert() throws SQLException {
        PreparedStatement pstmt = DbConnection.connection.prepareStatement(
            "INSERT INTO " + getTableName() + " (price_per_day, period) VALUES (?, ?)");
        pstmt.setInt(1, getPricePerDay());
        pstmt.setInt(2, getPeriod());

        pstmt.executeUpdate();
        pstmt.close();
        }
    
    public void update() throws SQLException {
        PreparedStatement pstmt = DbConnection.connection.prepareStatement(
          "UPDATE " + getTableName() + " SET price_per_day = ?, period = ? WHERE id = ? ");
        pstmt.setInt(1, getPricePerDay());
        pstmt.setInt(2, getPeriod());
        pstmt.setInt(3, getId());

        pstmt.executeUpdate();
        pstmt.close();
     }
    
    @Override
    public String toString() {
        return "jdbc.client.Price[ id=" + id + " ]";
    }
    
    public static String getTableName() {
        return tableName;
    }
}
