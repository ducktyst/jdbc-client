/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbdc.client;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class PriceController {
    private static String getTableName() {
        return Employee.getTableName();
    }

    public static Price getPrice(int id) throws SQLException {
        Price price = new Price();
        PreparedStatement statement = DbConnection.connection.prepareStatement(
                "SELECT * FROM " + getTableName() + " WHERE id = ?;");
        statement.setInt(1, id);

        ResultSet rs = statement.executeQuery();

        while (rs.next()) {
            price.setId(rs.getInt("id"));
            price.setPricePerDay(rs.getInt("price_per_day"));
            price.setPeriod(rs.getInt("period"));
        }
        return price;
    };

    public static void detelePrice(int id) throws SQLException {
        PreparedStatement pstmt = null;

        pstmt = DbConnection.connection.prepareStatement(
                "DELETE FROM " + getTableName() + " WHERE id=?");
        pstmt.setInt(1, id);
        pstmt.executeUpdate();

        pstmt.close();
    }

    public static List<Price> getAll() throws SQLException {
        List<Price> prices = new ArrayList(){};
        PreparedStatement pstmt = DbConnection.connection.prepareStatement(
                "SELECT * FROM " + getTableName());

        ResultSet rs = pstmt.executeQuery();

        while (rs.next()) {
            Price price = new Price();
            price.setId(rs.getInt("id"));
            price.setPricePerDay(rs.getInt("price_per_day"));
            price.setPeriod(rs.getInt("period"));

            prices.add(price);
        }
        return prices;
        
    }
}


// https://habr.com/ru/post/225189/
// https://habr.com/ru/company/redmadrobot/blog/275515/
// https://medium.com/@xufocoder/a-quick-intro-to-dependency-injection-what-it-is-and-when-to-use-it-de1367295ba8

//https://habr.com/ru/company/piter/blog/354532/

//https://habr.com/ru/post/349836/

//
//https://proselyte.net/tutorials/spring-tutorial-full-version/jdbc-in-spring/
//
//package net.proselyte.jdbc.util;
//
//import net.proselyte.jdbc.model.Developer;
//import org.springframework.jdbc.core.RowMapper;
//
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
//public class DeveloperMapper implements RowMapper{
//    @Override
//    public Developer mapRow(ResultSet rs, int rowNum) throws SQLException {
//        Developer developer = new Developer();
//        developer.setId(rs.getInt("id"));
//        developer.setName(rs.getString("name"));
//        developer.setSpecialty(rs.getString("specialty"));
//        developer.setExperience(rs.getInt("experience"));
//        return developer;
//    }
//}
//
