/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbdc.client;

import java.util.Date;

/**
 *
 * @author user
 */
public class Report {
    
    Date dateFrom;
    Date dateTo;
    int sum;
    
    Report () {}
    
    Report (Date from, Date to, int s) {
        dateFrom = from;
        dateTo = from;
        sum = s;
    }
    
    public void setDateFrom(Date d) {
        dateFrom = d;
    }
    
    public Date getDateFrom() {
        return dateFrom;
    }
    
    public void setDateTo(Date d) {
        dateTo = d;
    }
    
    public Date getDateTo() {
        return dateTo;
    }
    
    public void setSum(int s) {
        sum = s;
    }
    
    public int getSum() {
        return sum;
    }
}
