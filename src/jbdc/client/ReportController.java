/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbdc.client;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ReportController {

    static void getProfitFotYear() {
        
        
    }

    static public String getTableName() {
        return "room";
    }
    static List<String> getTableRoomsHeader() {
        List<String> res = new ArrayList();
        res.add("date");
        res.add("sum");
        return res;
    }
    
    static Report getReportForPeriod(Date from, Date to) throws SQLException {
        Report report = new Report(from, to, 0);
        // добавить group by month
        // фильтрация с 01.01 до 31.12 выбранного года
        PreparedStatement ps = DbConnection.connection.prepareStatement(
                "SELECT SUM(total_cost) as total_sum"
                + " FROM room_rent"
                + " WHERE rent_from >= ? AND rent_to <= ?");
        ps.setDate(1, (java.sql.Date) from);
        ps.setDate(2, (java.sql.Date) to);

        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            report.setSum(rs.getInt("total_sum"));
        }

        System.out.println(
            String.format("getReportForPeriod(Date from %s, Date to %s)" ,from, to) + ";"
            + ps.toString() + ";"
            + String.valueOf(report.getSum())
        );

        return report;
    }
}
