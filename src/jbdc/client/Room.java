/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbdc.client;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class Room implements Serializable {
    final static private String tableName = "room";

    private Integer id;
    private String number;
    private int class1;

    public Room() {
    }

    public Room(Integer id) {
        this.id = id;
    }

    public Room(Integer id, String number, int class1) {
        this.id = id;
        this.number = number;
        this.class1 = class1;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getClass1() {
        return class1;
    }

    public void setClass1(int class1) {
        this.class1 = class1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Room)) {
            return false;
        }
        Room other = (Room) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    
    public void insert() throws SQLException {
        PreparedStatement pstmt = DbConnection.connection.prepareStatement(
            "INSERT INTO " + getTableName() + " (number, class) VALUES (?, ?)");
        pstmt.setString(1, getNumber());
        pstmt.setInt(2, getClass1());

        pstmt.executeUpdate();
        pstmt.close();
    }
    
    public void update() throws SQLException {
        PreparedStatement pstmt = DbConnection.connection.prepareStatement(
          "UPDATE " + getTableName() + " SET number = ?, class = ? WHERE id = ?");

        pstmt.setString(1, getNumber());
        pstmt.setObject(2, getClass1());
        pstmt.setInt(3, getId());

        pstmt.executeUpdate();
        pstmt.close();
    }
    
    @Override
    public String toString() {
        return "jdbc.client.Room[ id=" + id + " ]";
    }
    
    public static String getTableName() {
        return tableName;
    }
}
