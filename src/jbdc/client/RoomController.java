/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbdc.client;

import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.swing.JTable;


public class RoomController {
    
    private static String getTableName(){
        return Room.getTableName();
    }

    static List<Integer> getRoomClasses() throws SQLException {
        List<Integer> classes = new ArrayList();

        PreparedStatement pstmt = DbConnection.connection.prepareStatement(
                "SELECT DISTINCT class FROM " + getTableName());

        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            classes.add(rs.getInt("class"));
        }
        System.out.println(
            String.format("getRoomClasses()") + ";"
            + pstmt.toString() + ";"
            + String.valueOf(classes)
        );
        return classes;
    }
    
    static List<Room> getFreeRoomsByClass(
        int rClass, java.util.Date dateFrom, java.util.Date dateTo) throws SQLException {
        List<Room> rooms = new ArrayList();
        
        List<Integer> busyRoomIds = RoomRentController.
                getRentedNumbersForDate(dateFrom, dateTo);
        if (busyRoomIds.isEmpty()) { // Костыль, чтобы не было пустого кортежа в NOT IN
            busyRoomIds.add(-9);
        }
        int[] busyrIds = UtilityMethods.toIntArray(busyRoomIds);

        PreparedStatement pstmt = DbConnection.connection.prepareStatement(
                "SELECT * FROM " + getTableName() + 
                " WHERE class = ? AND id NOT IN " +
                Arrays.toString(busyrIds).replace("[", "(").replace("]", ")"));
        pstmt.setInt(1, rClass);

        ResultSet rs = pstmt.executeQuery();
        List<Integer> room_ids = new ArrayList();
        while (rs.next()) {
            Room room = new Room();
            room.setId(rs.getInt("id"));
            room.setNumber(rs.getString("number"));
            room.setClass1(rs.getInt("class"));

            rooms.add(room);
            room_ids.add(room.getId());
        }

        System.out.println(
            String.format("getFreeRoomsByClass(int rClass %s, java.util.Date dateFrom %s, java.util.Date dateTo %s)", rClass, dateFrom, dateTo) + ";"
            + pstmt.toString() + ";"
            + String.valueOf(room_ids)
        );

        return rooms;
    }

    static List<Room> getFreeRoomsForDate(Date dateFrom, Date dateTo) throws SQLException {
        List<Room> rooms = new ArrayList(){};
        List<Integer> busyRoomIds = RoomRentController.
                getRentedNumbersForDate(dateFrom, dateTo);

        PreparedStatement pstmt = DbConnection.connection.prepareStatement(
                "SELECT * FROM " + getTableName() + "WHERE room_id NOT IN ?");

        Array array = DbConnection.connection.
                createArrayOf("INT", busyRoomIds.toArray());
        pstmt.setArray(1, array);

        ResultSet rs = pstmt.executeQuery();

        while (rs.next()) {
            Room room = new Room();
            room.setId(rs.getInt("id"));
            room.setNumber(rs.getString("number"));
            room.setClass1(rs.getInt("class"));

            rooms.add(room);
        }

        List<Integer> ids = new ArrayList();
        for (Room r : rooms) {
            ids.add(r.getId());
        }
        System.out.println(
            String.format("getFreeRoomsForDate(Date dateFrom, Date dateTo)", dateFrom, dateTo)+ ";"
            + pstmt.toString() + ";"
            + String.valueOf(ids)
        );

        return rooms;
    }

    public static Room newRoomFromJTable(JTable jTable) {
        Room room = new Room();

        String roomNumber = (String)jTable.getValueAt(0, 0);
        room.setNumber(roomNumber);

        int roomClass = Integer.parseInt((String)jTable.getValueAt(0, 1));
        room.setClass1(roomClass);
        return room;
    }

    public static Room getRoom(int id) throws SQLException {
        Room obj = new Room();

        PreparedStatement pstmt = DbConnection.connection.prepareStatement(
                "SELECT * FROM " + getTableName() + " WHERE id = ?");
        pstmt.setInt(1, id);

        ResultSet rs = pstmt.executeQuery();

        while (rs.next()) {
            obj.setId(rs.getInt("id"));
            obj.setNumber(rs.getString("number"));
            obj.setClass1(rs.getInt("class"));
        }

        return obj;
    };

    public static void deteleRoom(int id) throws SQLException {
        PreparedStatement pstmt = DbConnection.connection.prepareStatement(
                "DELETE FROM " + getTableName() + " WHERE id=?");
        pstmt.setInt(1, id);

        pstmt.executeUpdate();
        pstmt.close();
    }
    
    public static List<Room> getAll() throws SQLException {
        List<Room> rooms = new ArrayList(){};
        PreparedStatement pstmt = DbConnection.connection.prepareStatement(
                "SELECT * FROM " + getTableName());

        ResultSet rs = pstmt.executeQuery();

        while (rs.next()) {
            Room room = new Room();
            room.setId(rs.getInt("id"));
            room.setNumber(rs.getString("number"));
            room.setClass1(rs.getInt("class"));

            rooms.add(room);
        }

        pstmt.close();
        return rooms;
    }
}
