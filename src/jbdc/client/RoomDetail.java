/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbdc.client;

/* Подробная информация о комнате. Тариф
*/
public class RoomDetail {
    private int roomId ;
    private int pricePerDay;
    private int period;
    
    RoomDetail(int roomId, int pricePerDay, int period) {
        this.roomId = roomId;
        this.pricePerDay = pricePerDay;
        this.period = period;
    }
    
    public int getRoomId() {
        return roomId;
    }
    
    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }
        
    public int getPricePerDay() {
        return pricePerDay;
    }
    
    public void setPricePerDay(int pricePerDay) {
        this.pricePerDay = pricePerDay;
    }
        
    public int getPeriod() {
        return period;
    }
    
    public void setPeriod(int period) {
        this.period = period;
    }
    
    @Override
    public String toString() {
        return String.format("%s _ %s _ %s", roomId, pricePerDay, period);
    }
}
