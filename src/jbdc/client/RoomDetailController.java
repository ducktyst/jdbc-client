/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbdc.client;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class RoomDetailController {
    
     public static List<RoomDetail> getfreeRoomsDetailByClass(
             int roomClass, Date freeFrom, Date freeTo) throws SQLException {

        List<RoomDetail> tariffs = new ArrayList<RoomDetail>();

        PreparedStatement pstmt = DbConnection.connection.prepareStatement(
            "SELECT r.id as room_id, p.price_per_day price_per_day, p.period period"
            + " FROM room r"
            + " INNER JOIN room_price rp ON (r.id = rp.room_id)"
            + " INNER JOIN price p ON (rp.id = rp.id)"
            + " INNER JOIN room_rent rr ON (r.id = rr.room_id)"
            + " WHERE r.class = ? AND rr.rent_to < ? OR rr.rent_from > ?"
            + " LIMIT 1000");

        pstmt.setInt(1, roomClass);
        pstmt.setDate(2, UtilityMethods.convertDateToSqlDate(freeFrom));
        pstmt.setDate(3, UtilityMethods.convertDateToSqlDate(freeTo));
        
        System.out.println(pstmt.toString());

        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            tariffs.add(new RoomDetail(
                    rs.getInt("room_id"),
                    rs.getInt("price_per_day"),
                    rs.getInt("period")));
        }
        pstmt.close();
        
        
        System.out.println(
            String.format("getfreeRoomsDetailByClass(int roomClass, Date freeFrom, Date freeTo)", roomClass, freeFrom, freeTo) + ";" 
            + pstmt.toString() + ";" 
            + String.valueOf(tariffs.toString())
        );
        return tariffs;
    }
}
