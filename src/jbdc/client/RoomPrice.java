/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbdc.client;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class RoomPrice implements Serializable {
    final static private String tableName = "room_price";
    
    private Integer id;
    private Price price;
    private Room room;

    public RoomPrice() {
    }

    public RoomPrice(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RoomPrice)) {
            return false;
        }
        RoomPrice other = (RoomPrice) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    public void insert() throws SQLException {
        PreparedStatement ps = DbConnection.connection.prepareStatement(
              "INSERT INTO " + getTableName() + " (price_id, room_id) VALUES (?, ?)");
          ps.setInt(1, getPrice().getId());
          ps.setInt(2, getRoom().getId());

          ps.executeUpdate();
          ps.close();
    }
    
    public void update() throws SQLException {
        PreparedStatement ps = DbConnection.connection.prepareStatement(
          "UPDATE " + getTableName() + " SET price_id = ?, room_id = ?  WHERE id = ?");

        ps.setInt(1, getPrice().getId());
        ps.setInt(2, getRoom().getId());
        ps.setInt(3, getId());
        ps.executeUpdate();
        ps.close();
    }
    
    @Override
    public String toString() {
        return "jdbc.client.RoomPrice[ id=" + id + " ]";
    }
    
    public static String getTableName() {
        return tableName;
    }
}
