/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbdc.client;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author user
 */
public class RoomPriceController {

    private static String getTableName(){
        return RoomPrice.getTableName();
    }

    public static RoomPrice getRoomPrice(int id) throws SQLException {
        RoomPrice room_price = new RoomPrice();

        PreparedStatement statement = DbConnection.connection.prepareStatement(
                "SELECT * FROM " + getTableName() +" WHERE id = ?");
        statement.setInt(2, id);

        ResultSet rs = statement.executeQuery();

        while (rs.next()) {
            room_price.setId(rs.getInt("id"));
            Price price = PriceController.getPrice(rs.getInt("price_id"));
            room_price.setPrice(price);
            Room room = RoomController.getRoom(rs.getInt("room_id"));
            room_price.setRoom(room);
        }
        return room_price;
    };

    public static List<RoomPrice> getRoomPricesByRoom(Room room) throws SQLException {
        List<RoomPrice> room_prices = new ArrayList();

        PreparedStatement pstmt = DbConnection.connection.prepareStatement(
                "SELECT * FROM " + getTableName() +" WHERE room_id = ?");
        pstmt.setInt(1, room.getId());

        ResultSet rs = pstmt.executeQuery();

        while (rs.next()) {
            RoomPrice roomPrice = new RoomPrice();
            roomPrice.setId(rs.getInt("id"));
            Price price = PriceController.getPrice(rs.getInt("price_id"));
            roomPrice.setPrice(price);
            Room r = RoomController.getRoom(rs.getInt("room_id"));
            roomPrice.setRoom(r);

            room_prices.add(roomPrice);
        }

        List<Integer> ids = new ArrayList();
        for (RoomPrice rp : room_prices) {
            ids.add(rp.getId());
        }
        System.out.println(String.format("getRoomPricesByRoom(Room room %s)", room.toString()) + ";"
            + pstmt.toString() + ";"
            + String.valueOf(ids)
        );
        return room_prices;
    };
    
    public static void deteleRoomPrice(int id) throws SQLException {
        PreparedStatement ps = DbConnection.connection.prepareStatement(
                "DELETE FROM " + getTableName() + " WHERE id=?");
        ps.setInt(1, id);
        ps.executeUpdate();
        ps.close();
    }
}
