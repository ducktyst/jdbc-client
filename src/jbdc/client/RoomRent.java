/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbdc.client;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;


public class RoomRent implements Serializable {
    final static private String tableName = "room_rent";

    private Integer id;
    private String firstName;
    private String lastName;
    private String phoneNo;
    private Date rentFrom;
    private Date rentTo;
    private int totalCost;
    private Room roomId;

    public RoomRent() {
    }

    public RoomRent(Integer id) {
        this.id = id;
    }

    public RoomRent(Integer id, String firstName, String lastName, 
            String phoneNo, Date rentFrom, Date rentTo, int totalCost) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNo = phoneNo;
        this.rentFrom = rentFrom;
        this.rentTo = rentTo;
        this.totalCost = totalCost;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public Date getRentFrom() {
        return rentFrom;
    }

    public void setRentFrom(Date rentFrom) {
        this.rentFrom = rentFrom;
    }

    public Date getRentTo() {
        return rentTo;
    }

    public void setRentTo(Date rentTo) {
        this.rentTo = rentTo;
    }

    public int getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(int totalCost) {
        this.totalCost = totalCost;
    }

    public Room getRoomId() {
        return roomId;
    }

    public void setRoomId(Room roomId) {
        this.roomId = roomId;
    }
    
    public void insert() throws SQLException {
        PreparedStatement ps = null;

        ps = DbConnection.connection.prepareStatement(
            "INSERT INTO " + getTableName()
            + " (room_id, first_name, last_name, phone_no,"
            + " rent_from, rent_to, total_cost) "
            + " VALUES (?, ?, ?, ?, ?, ?, ?)");

          Room room = getRoomId();
          if (room == null) {
              throw new SQLException("Current room_id not exist");
          }
          ps.setInt(1, room.getId());
          ps.setString(2, getFirstName());
          ps.setString(3, getLastName());
          ps.setString(4, getPhoneNo());
          java.sql.Date rentFromSql = UtilityMethods.convertDateToSqlDate(getRentFrom());
          ps.setDate(5, rentFromSql);
          java.sql.Date rentToSql = UtilityMethods.convertDateToSqlDate(getRentTo());
          ps.setDate(6, rentToSql);
          ps.setInt(7, getTotalCost());    

          ps.executeUpdate();
          ps.close();
    }
    
    public void update() throws SQLException {
        PreparedStatement ps = DbConnection.connection.prepareStatement(
          "UPDATE " + getTableName() + " SET "
                  + "room_id = ?, "
                  + "first_name = ?, "
                  + "last_name = ?, "
                  + "phone_no = ?, "
                  + "rent_from = ?, "
                  + "rent_to = ?, "
                  + "total_cost = ? WHERE id = ?");

        Room room = getRoomId();
        if (room == null) {
            throw new SQLException("Current room_id not exist");
        } else {
            ps.setInt(1, room.getId());
        }

        ps.setString(2, getFirstName());
        ps.setString(3, getLastName());
        ps.setString(4, getPhoneNo());
        java.sql.Date rentFromSql = UtilityMethods.convertDateToSqlDate(getRentFrom());
        ps.setDate(5, rentFromSql);
        java.sql.Date rentToSql = UtilityMethods.convertDateToSqlDate(getRentTo());
        ps.setDate(6, rentToSql);
        ps.setInt(7, getTotalCost());    
        ps.setInt(8, getId());

        ps.executeUpdate();
        ps.close();
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RoomRent)) {
            return false;
        }
        RoomRent other = (RoomRent) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "jdbc.client.RoomRent[ id=" + id + " ]";
    }

    public static String getTableName() {
        return tableName;
    }
}
