/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbdc.client;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class RoomRentController {

    static List<Integer> getRentedNumbersForDate(Date dateFrom, Date dateTo) throws SQLException {
        List<Integer> freeIds = new ArrayList();
        PreparedStatement ps = DbConnection.connection.prepareStatement(
                "SELECT room_id FROM " + getTableName() 
                 + " WHERE rent_to < ? OR rent_from > ?");
        ps.setDate(1, UtilityMethods.convertDateToSqlDate(dateFrom));
        ps.setDate(2, UtilityMethods.convertDateToSqlDate(dateTo));

        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            freeIds.add(rs.getInt("room_id"));
        }
        ps.close();

        System.out.println(
            String.format("getRentedNumbersForDate(Date dateFrom %s, Date dateTo %s)", dateFrom, dateTo) + ";" 
            + ps.toString() + ";" 
            + String.valueOf(freeIds)
        );

        return freeIds;
    }
    
    public static RoomRent getRoomRent(int id) throws SQLException {
        RoomRent roomRent = new RoomRent();
        PreparedStatement ps = DbConnection.connection.prepareStatement(
                "SELECT * FROM " + getTableName() 
                + " WHERE id = ?");
        ps.setInt(2, id);

        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            roomRent.setId(rs.getInt("id"));
            Room room = RoomController.getRoom(rs.getInt("room_id"));
            roomRent.setRoomId(room);
            roomRent.setFirstName(rs.getString("first_name"));
            roomRent.setLastName(rs.getString("last_name"));
            roomRent.setPhoneNo(rs.getString("phone_no"));
            roomRent.setRentFrom(rs.getDate("rent_from"));
            roomRent.setRentTo(rs.getDate("rent_to"));
            roomRent.setTotalCost(rs.getInt("total_cost"));
        }

        return roomRent;
    };
    
    public static void deteleRoomRent(int id) throws SQLException {
        PreparedStatement ps = DbConnection.connection.prepareStatement(
                "DELETE FROM " + getTableName() + " WHERE id=?");
        ps.setInt(1, id);
        ps.executeUpdate();
        ps.close();
    }

    private static String getTableName(){
        return RoomRent.getTableName();
    }
}
