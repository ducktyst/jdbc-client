/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbdc.client;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UtilityMethods {
    static public java.util.Date convertStrToDate (String date) throws ParseException {
        String[] dateParts = date.split("-");
        if (dateParts.length != 3 ) {
            throw new ParseException(
                    "Дата должна быть YYYY-MM-DD", date.length());
        }

        int year = Integer.valueOf(dateParts[0]);
        int month = Integer.valueOf(dateParts[1]);
        int day = Integer.valueOf(dateParts[2]);

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        df.setLenient(false);
        java.util.Date d;
        try {
            d = df.parse(date);
        } catch (ParseException ex) {
            throw new ParseException("Неверное значение даты \"" + date + "\"", -1);
        }
        return d;
    }
    
    static public java.sql.Date convertDateToSqlDate (Date date) {
        int year = 1900 + date.getYear();
        int month = 1 + date.getMonth();
        int day = date.getDate();

        LocalDate localDate = LocalDate.of(year, month, day);
        java.sql.Date sqlDate = java.sql.Date.valueOf(localDate);
        return sqlDate;
    }
    
    static public void createEmployee(String selectedTable) {
        Employee employee = new Employee();

            employee.setFirstName(selectedTable);
            employee.setLastName(selectedTable);
            employee.setPhoneNo(selectedTable);
            employee.setPosition(selectedTable);

            java.util.Date employmentDate = null;
            try {
                String valueDate = "2038-03-03";
                employmentDate = UtilityMethods.convertStrToDate(valueDate);
            } catch (ParseException ex) {
                JDBCClient.setStatusMessage(ex.getMessage());
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                return;
            }            

            employee.setEmploymentDate(employmentDate);
    }

    static public int[] toIntArray(List<Integer> list){
        int[] ret = new int[list.size()];
        for(int i = 0;i < ret.length;i++)
            ret[i] = list.get(i);
        return ret;
}
}
