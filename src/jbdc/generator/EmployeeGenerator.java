/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbdc.generator;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import jbdc.client.Employee;


public class EmployeeGenerator {
    static String[] firstNames = new String[]{
        "Pier", "Jorsch", "Petr", "Vladislav", "Sergey", "Gena", "Alexander"
    };
    static String[] lastNames = new String[] {
      "Petrarka" , "Zelenov", "Bokkacho", "Jotto", "Vremenev", "Kuptsov"
    };
    
    public static List<Employee> generate(int count) {
        List<Employee> instances = new ArrayList();
        for (int i = 0; i < count; i++) {
            int id = i;
            String firstName = firstNames[(int)(Math.random() * (firstNames.length-1))];
            String lastName = lastNames[(int)(Math.random() * (lastNames.length-1))];
            String phoneNo = "8952" + (int)(Math.random()*10);
            
            String position = "position";

            Date employmentDate = new Date(); 
            Employee instance = new Employee(
                    id, firstName, lastName, phoneNo, position, employmentDate);
            instances.add(instance);
        }
        return instances;
    }
}
