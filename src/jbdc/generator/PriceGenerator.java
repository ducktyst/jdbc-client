/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbdc.generator;

import java.util.ArrayList;
import java.util.List;

import jbdc.client.Price;


public class PriceGenerator {
    static int[] periods = new int[] {1, 4, 7, 14};
    
    public static List<Price> generate(int count) {
        List<Price> instances = new ArrayList();
        for (int i = 1; i <= count; i++) {
            int id = i;
            int pricePerDay = (int) (Math.random() * 100);
            int period = periods[(int)(Math.random() * (periods.length - 1))+1];
            Price instance = new Price(id, pricePerDay, period);
            
            instances.add(instance);
        }
        return instances;
    }

    private Object Random() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
