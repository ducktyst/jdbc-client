/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbdc.generator;

import java.util.ArrayList;
import java.util.List;

import jbdc.client.Room;


public class RoomGenerator {
   
    public static List<Room> generate(int count) {
        List<Room> instances = new ArrayList();
        for (int i = 1; i <= count; i++) {
            int id = i;
            int numInt = (int) (Math.random() * 10);
            String number = "1" + numInt;
            int class1 = (int) (Math.random() * 3) + 3;

            Room instance = new Room(id, number, class1);
            instances.add(instance);
        }
        return instances;
    }
}
