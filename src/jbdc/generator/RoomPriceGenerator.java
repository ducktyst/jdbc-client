/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbdc.generator;

import java.util.ArrayList;
import java.util.List;
import jbdc.client.Price;

import jbdc.client.Room;
import jbdc.client.RoomPrice;


public class RoomPriceGenerator {
    
    public static List<RoomPrice> generate(int count) {
        List<RoomPrice> instances = new ArrayList();
        for (int i = 1; i <= count; i++) {
            int id = i;
            Room room = RoomGenerator.generate(1).get(0);
            Price price = PriceGenerator.generate(1).get(0);

            room.setId((int) (Math.random() * 9999) + 1);
            price.setId((int) (Math.random() * 99) + 1);
            
            RoomPrice instance = new RoomPrice(id);
            instance.setRoom(room);
            instance.setPrice(price);
            
            instances.add(instance);
        }
        return instances;
    }
}
