/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbdc.generator;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import jbdc.client.Room;
import jbdc.client.RoomRent;


public class RoomRentGenerator {
    static String[] firstNames = new String[]{
        "Pier", "Jorsch", "Petr", "Vladislav", "Sergey", "Gena", "Alexander"
    };
    static String[] lastNames = new String[] {
      "Petrarka" , "Zelenov", "Bokkacho", "Jotto", "Vremenev", "Kuptsov"
    };
    
    public static List<RoomRent> generate(int count) {
        List<RoomRent> instances = new ArrayList();
        for (int i = 1; i <= count; i++) {
            int id = i;
            Room room = RoomGenerator.generate(1).get(0);

            String firstName = firstNames[(int)(Math.random() * (firstNames.length-1))];
            String lastName = lastNames[(int)(Math.random() * (lastNames.length-1))];
            String phoneNo = "8952" + (int)(Math.random()*100);

            Date rentFrom = new Date();
            Date rentTo = new Date();

            // FIXME: Взять из базы RoomPrice, Price и сгенерировать
            // по реальным значениям
            int totalCost = (int) (Math.random() * 5000);

            RoomRent instance = new RoomRent(id, firstName, lastName,
                phoneNo, rentFrom, rentTo, totalCost);
            instance.setRoomId(room);
            instances.add(instance);
        }
        return instances;
    }
}
