/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jbdc.generator;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jbdc.client.DbConnection;
import jbdc.client.Employee;
import jbdc.client.Price;
import jbdc.client.Room;
import jbdc.client.RoomPrice;
import jbdc.client.RoomRent;



public class TestInsert {
    static Connection conn = null;
    static int insertCount = 10000;
    
    public static void initConnection() throws SQLException {
        // TODO: создание бд для вставки и удаление в коде, а не руками
       
        DbConnection.closeConnection();
        DbConnection.newConnection("localhost", 5432, "test_db", "postgres", "postgres");
        conn = DbConnection.connection;
    }
        
    public static void main(String[] args) {
       //testAutoCommitOff();
        testAutocommitOn();
    }
    
    public static void testAutoCommitOff() {
        try {
            long begin;
            long end;
            long diff;
            
            initConnection();
  
            System.out.println("Автокоммит Выкл.");
            //System.out.println("После добавления индексов");
            System.out.println("Индексы: Нет");
            conn.setAutoCommit(false);
            testInsertEmloyees();
            conn.commit();

            testInsertPrices();
            conn.commit();

            testInsertRooms();
            conn.commit();

            testInsertRoomPrices();
            conn.commit();

            testInsertRoomRents();
            conn.commit();

        } catch (SQLException ex) {
            Logger.getLogger(TestInsert.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void testAutocommitOn() {
        try {
            long begin;
            long end;
            long diff;
            
            initConnection();
  
            System.out.println("Автокоммит Вкл.");
            //System.out.println("После добавления индексов");
            System.out.println("Индексы нет");
            conn.setAutoCommit(true);
            testInsertEmloyees();

            testInsertPrices();

            testInsertRooms();

            testInsertRoomPrices();

            testInsertRoomRents();

        } catch (SQLException ex) {
            Logger.getLogger(TestInsert.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void testInsertEmloyees() throws SQLException {
        long begin;
        long end;
        long diff;

        List<Employee> objects = EmployeeGenerator.generate(insertCount);

        begin = System.nanoTime();
        for (Employee obj: objects) {
            try {
                obj.insert();
            } catch (SQLException ex) {
                Logger.getLogger(TestInsert.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
        end = System.nanoTime();
        diff = end - begin;
        System.out.println(String.format("%s объектов %s вставлено за %s нс",
                insertCount, "Employee", diff));
    }

    public static void testInsertPrices() throws SQLException {
        long begin;
        long end;
        long diff;

        List<Price> objects = PriceGenerator.generate(insertCount);
        
        begin = System.nanoTime();
        for (Price obj: objects) {
            try {
                obj.insert();
            } catch (SQLException ex) {
                Logger.getLogger(TestInsert.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
        end = System.nanoTime();
        diff = end - begin;
        System.out.println(String.format("%s объектов %s вставлено за %s нс",
                insertCount, "Price", diff));
    }

    public static void testInsertRooms() throws SQLException {
        long begin;
        long end;
        long diff;


        List<Room> objects = RoomGenerator.generate(insertCount);
        
        begin = System.nanoTime();
        for (Room obj: objects) {
            obj.insert();
        } 
        end = System.nanoTime();
        diff = end - begin;
        System.out.println(String.format("%s объектов %s вставлено за %s нс",
                insertCount, "Room", diff));
    }

    public static void testInsertRoomPrices() throws SQLException {
        long begin;
        long end;
        long diff;


        List<RoomPrice> objects = RoomPriceGenerator.generate(insertCount);
        
        begin = System.nanoTime();
        for (RoomPrice obj: objects) {
            obj.insert();
        } 
        end = System.nanoTime();
        diff = end - begin;
        System.out.println(String.format("%s объектов %s вставлено за %s нс",
                insertCount, "RoomPrice", diff));
    }
    
    public static void testInsertRoomRents() throws SQLException {
        long begin;
        long end;
        long diff;

        List<RoomRent> objects = RoomRentGenerator.generate(insertCount);

        begin = System.nanoTime();
        for (RoomRent obj: objects) {
            obj.insert();
        }
        end = System.nanoTime();
        diff = end - begin;
        System.out.println(String.format("%s объектов %s вставлено за %s нс.",
                insertCount, "RoomRent", diff));
    }
}
